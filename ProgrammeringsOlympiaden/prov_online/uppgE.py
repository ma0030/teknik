
N, K = map(int, input().split())
painting = list(map(int, input().split()))
minTime = 0
currentArry = []

for i in range(K):
    currentArry.append([])
    currentArry[i].append(painting[i])
    currentArry[i].append(i)

if N != K:
    for i in range(K, N):
        temp = painting[i]+(i-currentArry[len(currentArry)-1][1])
        tempArry = []
        for a in range(len(currentArry)):
            tempArry.append([])
            if a == 0:
                tempArry[a].append(
                    (currentArry[a][0]+(currentArry[a+1][1]-currentArry[a][1]))-temp)
                tempArry[a].append(a)
            elif a == len(currentArry)-1:
                tempArry[a].append(
                    (currentArry[a][0]-(currentArry[a][1]-currentArry[a-1][1]))-temp)
                tempArry[a].append(a)

            else:
                tempArry[a].append(currentArry[a][0]-temp)
                tempArry[a].append(a)
        tempArry = sorted(tempArry, key=lambda l: l[0], reverse=True)
        if tempArry[0][0] > 0:
            currentArry.append([])
            currentArry[len(currentArry)-1].append(painting[i])
            currentArry[len(currentArry)-1].append(i)
            currentArry.pop(tempArry[0][1])


for item in currentArry:
    minTime += item[0]
minTime += currentArry[(len(currentArry)-1)][1]-currentArry[0][1]

print(minTime)
