
N = (int)(input())
winF = 0
winS = 0
start = False
diffranse = 0
arry = []


for item in range(N):
    F, S = map(int, input().split())
    arry.append([])

    if F < S:
        arry[item].append("S")
    elif F > S:
        arry[item].append("F")
    else:
        arry[item].append("L")
    arry[item].append(abs(F-S))


arry = sorted(arry, key=lambda l: l[1], reverse=True)

for i in range(len(arry)):

    if arry[i][0] == "S":
        winS += 1
    elif arry[i][0] == "F":
        winF += 1

    if winF-winS >= diffranse:
        start = True

        diffranse = winF-winS
        if i == len(arry)-1:
            number = 0
        else:
            number = arry[i+1][1]
    elif start == False:
        start = True
        number = arry[i][1]
print(arry)
print(int(number))
