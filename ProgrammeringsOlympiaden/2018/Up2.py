minRutor, maxRutor = map(int, input().split())

utval = []
for i in range(minRutor, maxRutor+1):

    for L in range(1, i):
        for B in range(int(i**0.5)+2):
            if L * B == i:
                utval.append([B, L])
utval.append([1, minRutor])
bestIndex = 0
bestValue = 999999999999
for i in range(len(utval)):
    lenght = int(utval[i][0] - utval[i][1])

    if lenght < 0:
        lenght = int(lenght*(-1))
    if bestValue > lenght:
        bestValue = lenght
        bestIndex = i

print(utval[bestIndex][0], utval[bestIndex][1])
